RiseupVPN
=========

Anonymous VPN. Easy, fast, secure.

This repo has everything needed to build Bitmask VPN/RiseupVPN on different
platforms (windows, mac and linux).

RiseupVPN is a branded build of Bitmask VPN. Bitmask VPN is a minimal rewrite
of the Bitmask VPN Client, written in golang, that for now lacks client
authentication, and is preconfigured to use a single provider. As of this
moment, it is built for a single provider (riseup.net), but it should be easy
to customize for other providers.


Install
------------

Linux
~~~~~

For GNU/Linux, RiseupVPN is distributed using snap. You can get the stable version of the snap from the `snap store` if you're using Gnome Software, that has desktop integration for snaps. Otherwise, you can use the terminal::


  sudo apt install snapd
  sudo snap install riseup-vpn --classic


After that, you *should* have a RiseupVPN entry in your menu. You can also launch the snap from a terminal::

  snap run riseup-vpn.launcher

Troubleshooting
~~~~~~~~~~~~~~~

In case you don't see the RiseupVPN systray icon, chances are that you don't have the legacy systray available in your desktop. You can get it back by installing an extension like `topicons-plus`_. Note that you have to enable under "Tweaks" for every user in your system that needs it.

.. _`topicons-plus`: https://extensions.gnome.org/extension/1031/topicons/

Testing other releases of the snap under Linux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To install a snap published on a specific channel, indicate it from the commandline::

  sudo snap install riseup-vpn --classic --candidate
  sudo snap install riseup-vpn --classic --beta
  sudo snap install riseup-vpn --classic --edge

The beta channel has four daily builds of whatever is in the master branch, for amd64 and i686. The edge channel usually contains fixes that we push to be tested individually.

If you are installing a local build of the snap, you need to add the ``--dangerous`` flag::

  sudo snap install riseupvpn --dangerous --classic riseup-vpn*.snap


Windows
~~~~~~~

We build a windows installer for RiseupVPN, using nsis. For testing, you can find the `snap artifact`_ for the latest build in the master branch in the CI. We publish the `latest stable windows installer`_, as well as `windows pre-releases`_.

.. _`snap artifact`: https://0xacab.org/leap/bitmask-vpn/-/jobs/artifacts/master/download?job=win_installer
.. _`latest stable windows installer`: https://downloads.leap.se/RiseupVPN/windows/RiseupVPN-win-latest.exe
.. _`windows pre-releases`: https://downloads.leap.se/RiseupVPN/windows/beta/

OSX
~~~

For OSX there is also a `.pkg` installer. You can always get the `latest stable OSX installer`_, as well as `osx pre-releases`_.

.. _`latest stable OSX installer`: https://downloads.leap.se/RiseupVPN/osx/RiseupVPN-OSX-latest.pkg
.. _`osx pre-releases`: https://downloads.leap.se/RiseupVPN/osx/beta/

Bugs
------------
You can report issues in `our bug tracker`_.

.. _`our bug tracker`: https://0xacab.org/leap/riseup_vpn/issues

Build
------------

If you want to build RiseupVPN yourself, get all prerequisites, using your system's package manager if you wish::

* golang
* make
* python3

Snap
~~~~

You can build a local version of the snap::

  make build_snap

OSX
~~~

To build RiseupVPN in OSX::

  make build_osx

Windows builds
~~~~~~~~~~~~~~

In *windows* there are a couple of extra dependencies. We recommend using chocolatey to get them::

* nsis
* nssm

You can do a native build in windows::

  make deps_win
  make openvpn_win
  make build_win

You can also cross-compile openvpn::

  # TODO - modify to build statically and with polarssl.

  apt install gcc-mingw-w64-x86-64
  git clone https://github.com/OpenVPN/openvpn-build
  cd openvpn-build/windows-nsis
  CHOST=x86_64-w64-mingw32 CBUILD=i686-pc-cygwin ./build

Hacking
---------------

If you want to contribute to Bitmask VPN/RiseupVPN, you can get in contact with us in the ``#leap`` channel at freenode.

You might also want to have a look at some related pieces that are used to build these packages.

* `bitmask-vpn`_: this is the golang implementation of RiseupVPN. It is
  basically a wrapper around openvpn, with knowledge of what configuration
  files are expected to exist in a LEAP provider. The only user interface is a minimalistic systray that uses libappindicator.
* `the bitmask helper`_: it lives in bitmask-vpn for Windows and osX. It implements a long-lived helper that runs with administrative privileges, used in OSX and Windows for launching openvpn and the firewall. In OSX it is run as a LaunchDaemon, and in Windows we use nssm to run this helper. It communicates with Bitmask VPN via a local http service.
* `bitmask-root`_: for the snaps, in GNU/Linux we use a one-shot privileged helper that relies on policykit to elevate privileges without asking for password each time, instead of the long-lived helper that we use in osx and windows packages (although there is a linux implementation, and this long-lived helper might make sense in some cases, we want to explore that. For instance for OpenWRT packages).

  In case you are wondering what is the relationship of Bitmask VPN and the regular Bitmask, it is planned that the bitmask client will at some point switch to use the same helpers. This repo probably will be turned into a generic packaging and helper tool to be used for both branded builds of BitmaskLite, and packaging of the frozen python payloads generated from bitmask-dev.

.. _`bitmask-vpn`: https://0xacab.org/leap/bitmask-vpn
.. _`the bitmask helper`: https://0xacab.org/leap/bitmask-vpn/tree/master/pkg/helper/
.. _`bitmask-root`: https://0xacab.org/leap/bitmask-dev/blob/master/src/leap/bitmask/vpn/helpers/linux/bitmask-root
