APPNAME := riseup-vpn
SYSTRAY := 0xacab.org/leap/bitmask-vpn
STAGING := staging
SYSTRAY_BIN := bitmask-vpn
HELPER_BIN := bitmask_helper
APP_NAME := RiseupVPN
BUILD_RELEASE?=no
WIN_CERT_PATH?=z:\leap\LEAP.pfx
WIN_CERT_PASS?=
OSX_CERT = "Developer ID Installer: LEAP Encryption Access Project"
VERSION = $(shell git -C `go env GOPATH`/src/$(SYSTRAY)  describe --tags --always)

TGZ_PATH = $(shell pwd)/dist/$(APPNAME)-$(VERSION)
tgz:
	# XXX not needed anymore
	mkdir -p $(TGZ_PATH)
	git -C `go env GOPATH`/src/$(SYSTRAY) archive HEAD | tar -x -C $(TGZ_PATH)
	mkdir -p $(TGZ_PATH)/helpers
	wget -O $(TGZ_PATH)/helpers/bitmask-root https://0xacab.org/leap/bitmask-dev/raw/master/src/leap/bitmask/vpn/helpers/linux/bitmask-root
	chmod +x $(TGZ_PATH)/helpers/bitmask-root
	wget -O $(TGZ_PATH)/helpers/se.leap.bitmask.policy https://0xacab.org/leap/bitmask-dev/raw/master/src/leap/bitmask/vpn/helpers/linux/se.leap.bitmask.policy
	cd dist; tar cvzf $(APPNAME)-$(VERSION).tgz $(APPNAME)-$(VERSION)
	rm -r $(TGZ_PATH)

# -----------------------------------------------------------------------------
# Windows 
# -----------------------------------------------------------------------------
CROSS_FLAGS = CGO_ENABLED=1 GOARCH=386 GOOS=windows CC="/usr/bin/i686-w64-mingw32-gcc" CGO_LDFLAGS="-lssp" CXX="i686-w64-mingw32-c++"

deps_win:
	choco install -y golang python nssm nsis wget 7zip
openvpn_win:
	if not exist staging\openvpn mkdir staging\openvpn
	wget https://build.openvpn.net/downloads/releases/latest/tap-windows-latest-stable.exe -O staging/openvpn/tap-windows.exe
# eventually, this should be built statically and cross compiled in the same pipeline that we build the installer. 
	wget https://downloads.leap.se/thirdparty/windows/openvpn-x86_64-w64-mingw32.tar.bz2 -O staging/openvpn/openvpn.tar.bz2
	7z e -y -ostaging/openvpn/ staging/openvpn/openvpn.tar.bz2
	7z e -y -r -ostaging/openvpn/ staging/openvpn/openvpn.tar *.dll
	7z e -y -r -ostaging/openvpn/ staging/openvpn/openvpn.tar *.exe
	copy .\staging\openvpn\openvpn.exe .\staging
	copy .\staging\openvpn\*.dll .\staging
openvpn_cross_win:
	mkdir -p staging/openvpn
	wget https://build.openvpn.net/downloads/releases/latest/tap-windows-latest-stable.exe -O $(STAGING)/openvpn/tap-windows.exe
	wget https://downloads.leap.se/thirdparty/windows/openvpn-x86_64-w64-mingw32.tar.bz2 -O $(STAGING)/openvpn/openvpn.tar.bz2
	tar xvjf $(STAGING)/openvpn/openvpn.tar.bz2 -C $(STAGING)/openvpn/
	cp $(STAGING)/openvpn/bin/openvpn.exe $(STAGING)/openvpn
	cp $(STAGING)/openvpn/bin/*.dll $(STAGING)
	cp $(STAGING)/openvpn/lib/engines-1_1/*.dll $(STAGING)
helper_win:
	go build -ldflags "-s -w" -o $(STAGING)/$(HELPER_BIN).exe $(SYSTRAY)/cmd/bitmask-helper
systray_win:
	go get -u $(SYSTRAY)/cmd/bitmask-vpn
	powershell '$$gopath=go env GOPATH;$$version=git -C $$gopath/src/$(SYSTRAY) describe --tags; go build -ldflags "-H windowsgui -s -w -X main.version=$$version" -o $(STAGING)/$(SYSTRAY_BIN).exe $(SYSTRAY)/cmd/bitmask-vpn'
build_win: staging\nssm.exe helper_win systray_win
# since it's tedious, I assume you did bootstrap openvpn_win manually already.
	echo "[+] building windows"
	if not exist dist mkdir dist
	powershell '$$gopath=go env GOPATH;$$version=git -C $$gopath/src/$(SYSTRAY) describe --tags; $(MAKE) -C win VERSION=$$version'
	"C:\Program Files (x86)\NSIS\makensis.exe" win/RiseupVPN-installer.nsi
sign_win:
	echo "[+] signing windows build"
	python win/sign.py $(WIN_CERT_PATH) $(WIN_CERT_PASS)
build_cross_win: staging/nssm.exe
	echo "!define VERSION $(VERSION)" > $(STAGING)/version.nsh
	go get 0xacab.org/leap/bitmask-vpn/cmd/bitmask-helper
	go get 0xacab.org/leap/bitmask-vpn/cmd/bitmask-vpn
	$(CROSS_FLAGS) $(MAKE) helper_win
	$(CROSS_FLAGS) go get $(SYSTRAY)/cmd/bitmask-vpn
	$(CROSS_FLAGS) go build -ldflags "-H windowsgui -s -w -X main.version=$(VERSION)" -o $(STAGING)/$(SYSTRAY_BIN).exe $(SYSTRAY)/cmd/bitmask-vpn
	mkdir -p dist
	make -C win VERSION=$(VERSION)
	makensis win/RiseupVPN-installer.nsi

# -----------------------------------------------------------------------------
# OSX
# -----------------------------------------------------------------------------

deps_osx:
# TODO - bootstrap homebrew if not there
	brew install python golang make upx
openvpn_osx:
	echo "[+] downloading openvpn..."
	wget -O $(STAGING)/openvpn https://downloads.leap.se/thirdparty/osx/openvpn/openvpn
helper_osx:
	go get -u github.com/sevlyar/go-daemon
	go build -ldflags "-s -w" -o $(STAGING)/$(HELPER_BIN) $(SYSTRAY)/cmd/bitmask-helper
	upx $(STAGING)/$(HELPER_BIN)
systray_osx:
	go get -u $(SYSTRAY)/cmd/bitmask-vpn
	go build -ldflags "-X main.version=$(VERSION)" -o $(STAGING)/$(SYSTRAY_BIN) $(SYSTRAY)/cmd/bitmask-vpn
	upx $(STAGING)/$(SYSTRAY_BIN)
bundle_osx:
	mkdir -p dist
	make -C osx VERSION=$(VERSION)
pkg_osx:
	osx/quickpkg --output dist/$(APP_NAME)-$(VERSION)_unsigned.pkg --scripts osx/scripts/ dist/$(APP_NAME).app/
	@if [ $(BUILD_RELEASE) = no ]; then\
		echo "[!] BUILD_RELEASE=no, skipping signature";\
	else\
		echo "[+] Signing the bundle";\
		productsign --sign $(OSX_CERT) dist/$(APP_NAME)-$(VERSION)_unsigned.pkg dist/$(APP_NAME)-$(VERSION).pkg;\
	fi
build_osx: openvpn_osx helper_osx systray_osx bundle_osx pkg_osx
	echo "[+] building osx..."

# -----------------------------------------------------------------------------
# Linux 
# -----------------------------------------------------------------------------

build_snap:
	echo "[+] building snap..."
	snapcraft build
	snapcraft snap
	mkdir -p dist
	mv $(APPNAME)* dist/

testfoo:
	dch -v $(VERSION) -M "debian package generated from the git repository";\

build_deb:  tgz
	echo "[+] building deb..."
	@if [ $(BUILD_RELEASE) = no ]; then\
		dch -v $(VERSION) -M "debian package generated from the git repository" && echo "[!] BUILD_RELEASE=no, incrementing changelog";\
	else\
		echo "[!] BUILD_RELEASE";\
	fi
	mkdir -p build
	cp dist/$(APPNAME)-$(VERSION).tgz build/$(APPNAME)_$(shell echo ${VERSION} | cut -d '-' -f 1-2).orig.tar.gz
	cd build && tar xzf $(APPNAME)_$(shell echo ${VERSION} | cut -d '-' -f 1-2).orig.tar.gz
	cp -r debian/ build/$(APPNAME)-$(VERSION)/
	cd build/$(APPNAME)-$(VERSION) && debuild -us -uc
	cp build/*.deb dist/
	git checkout -- debian/changelog

# -----------------------------------------------------------------------------
# Utils
# -----------------------------------------------------------------------------

clean:
	rm -rf dist/ build/

staging\nssm.exe:
	xcopy /y "C:\ProgramData\chocolatey\lib\NSSM\tools\nssm.exe" $(STAGING)
staging/nssm.exe:
	wget https://nssm.cc/release/nssm-2.24.zip -O $(STAGING)/nssm.zip
	unzip $(STAGING)/nssm.zip -d $(STAGING)
	mv $(STAGING)/nssm-*/win32/nssm.exe $(STAGING)
	rm -rf $(STAGING)/nssm-* $(STAGING)/nssm.zip
