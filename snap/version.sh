#!/bin/sh
git clone https://0xacab.org/leap/bitmask-vpn 2> /dev/null > /dev/null
git -C bitmask-vpn describe --tags --always | tee snap/version.txt
rm -rf bitmask-vpn
