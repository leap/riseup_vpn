Changelog for Bitmask Lite/RiseupVPN
====================================

0.19.2 - `master`_
-------------------
.. note:: This version is not yet released and is under active development.

0.19.1
-------------------
- Use firewall status to check if the vpn is in failed status
- Create a tarball for packaging.
- Reload firewall with SIGUSR1
- Reorganize code
- Move provider configuration into a set of constants
- Vendorize deps
- Wait until the systray is initalized to start the systray loop
- Allow tls 1.0 as a workaround for buster/sid
- Rename the systray reconnection on fail-close

0.18.12
-------
...

0.18.11
-------

Features
~~~~~~~~
- Generate messages.json for transifex.
- Add french and dutch translations.

Bugfixes
~~~~~~~~
- Fix wrong order for menus in windows.
- Add the right executable to the snap autostart
- Avoid menu flickering in windows 10.

0.18.10
---------------------

Bugfixes
~~~~~~~~
- `riseupvpn/#27 <https://0xacab.org/leap/riseup_vpn/issues/27>`_: Fix version reporting in snap.
- `riseupvpn/#31 <https://0xacab.org/leap/riseup_vpn/issues/31>`_: Workaround for desktop entry not showing up on debian.
- `bitmask-systray/#60 <https://0xacab.org/leap/bitmask-systray/issues/60>`_: Fix too many temporal files (vendoring upstream pr).
- `bitmask-systray/#72 <https://0xacab.org/leap/bitmask-systray/issues/72>`_: Fix gateway selection by timezone.
- vendor getlantern/systray library.


0.18.9
--------------------

- Initial release (windows, osx and snap packages).

.. _`master`: https://0xacab.org/leap/riseup_vpn
